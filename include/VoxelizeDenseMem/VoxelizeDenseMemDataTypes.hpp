#pragma once

#include <CL/cl.hpp>
#include "OCLInfra/Buffer.hpp"


namespace ocla {

	template <typename Position>
	struct AABB {
		Position min;
		Position max;
	};

	template <typename Block>
	struct VoxelGridBuffer {
		using block_type = Block;
		BufferWrapper<block_type> voxelGrid;
		cl_uint3 gridDimensions;
		AABB<cl_float3> worldBoundBox;
		cl_float3 unitVoxelDimensions;
	};

} // namespace ocla