
#pragma once

#include <CL/cl.hpp>
#include <vector>

namespace ocla {

	std::vector<cl_int3> extractIndividualVoxels(const std::vector<cl_uint>& voxelGrid, const size_t numVoxels, const size_t axisResolution);
	cl_int3 determineVoxelCoord(cl_int location_1d, const size_t axisResolution);

} // ocla