
#pragma once

#include <array>
#include <vector>

#include <CL/cl.hpp>

#include "OCLInfra/Buffer.hpp"

#include "VoxelizeDenseMem/VoxelizeDenseMemDataTypes.hpp"

namespace ocla {

	class SurfaceVoxelizeDenseMem {
	public:
		SurfaceVoxelizeDenseMem(cl::Context& context, cl::Device& device, cl::CommandQueue& commandQueue);

		VoxelGridBuffer<cl_uint> voxelize(const BufferWrapper<cl_float>& d_triangles, const cl_uint numTriangles,
			const cl_uint3 gridDimensions, const AABB<cl_float3> worldBoundingBox, const cl_float3 unitVoxelDimensions);
		void voxelize(const BufferWrapper<cl_float>& d_triangles, const cl_uint numTriangles,
			VoxelGridBuffer<cl_uint>& voxelGrid);
	private:
		cl::Context& context;
		cl::Device& device;
		cl::CommandQueue& commandQueue;

		const size_t computeUnits = 24;
		const size_t processorDensity = 64; // number of processors per compute unit
		const size_t processors = computeUnits * processorDensity;

		// kernels
		cl::Kernel k_voxelization;
	};

	class DenseSolidVoxelizeDenseMem {
	public:
		DenseSolidVoxelizeDenseMem(cl::Context& context, cl::Device& device, cl::CommandQueue& commandQueue);

		VoxelGridBuffer<cl_uint> voxelize(const BufferWrapper<cl_float>& d_triangles, const cl_uint numTriangles,
			const cl_uint3 gridDimensions, const AABB<cl_float3> worldBoundingBox, const cl_float3 unitVoxelDimensions);
	private:
		cl::Context& context;
		cl::Device& device;
		cl::CommandQueue& commandQueue;

		const size_t computeUnits = 24;
		const size_t processorDensity = 64; // number of processors per compute unit
		const size_t processors = computeUnits * processorDensity;

		// kernels
		cl::Kernel k_voxelization;
	};

} // namespace ocla