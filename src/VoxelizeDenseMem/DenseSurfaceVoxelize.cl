
typedef struct {
    float3 min;
    float3 max;
} AABB_f;

typedef struct {
    int3 min;
    int3 max;
} AABB_i;

inline uint bitBlockLocalIndex(uint location) {
#ifdef BLOCK_INDEXING_RTL
	return (uint)(31) - (location % (uint)(32)); // we count bit positions RtL, but array indices LtR
#else
	return location % (uint)(32); // we count bit positions LtR, and array indices LtR
#endif
}

inline uint blockIndex(uint location) {
	return location / 32;
}

void setBit(__global uint* voxel_table, uint location) {
	uint bit_pos = bitBlockLocalIndex(location);
	uint mask = 1 << bit_pos;
	atomic_or(&(voxel_table[blockIndex(location)]), mask);  // location / 32 == block index
}

// [Schwarz and Seidel 2010]: Conservative surface voxelization using dense memory storage
__kernel void denseSurfaceVoxelize(__constant AABB_f* bbox, __constant uint3* gridsize, __constant uint* n_triangles,
	__constant float3* unit, __global float* triangle_data, __global uint* voxel_table) {
	
    uint thread_id = get_global_id(0);
	uint stride = get_global_size(0);

	// Common variables used in the voxelization process
	float3 delta_p = (float3)(unit[0].x, unit[0].y, unit[0].z);
	float3 critical_point = (float3)(0.0f, 0.0f, 0.0f);

	while (thread_id < *n_triangles) { // Every thread works on specific triangles in its stride
		uint t = thread_id * 9; // A triangle contains 9 floats, 3 vertices of 3 components

		// Move vertices to origin using bbox
		float3 v0 = (float3)(triangle_data[t], triangle_data[t + 1], triangle_data[t + 2]) - bbox->min;
		float3 v1 = (float3)(triangle_data[t + 3], triangle_data[t + 4], triangle_data[t + 5]) - bbox->min; 
		float3 v2 = (float3)(triangle_data[t + 6], triangle_data[t + 7], triangle_data[t + 8]) - bbox->min;

        // Edge vectors
		float3 e0 = v1 - v0;
		float3 e1 = v2 - v1;
		float3 e2 = v0 - v2;
		// Normal vector pointing up from the triangle
		float3 triangleNormal = normalize(cross(e0, e1));

		AABB_i t_bbox_grid;
        {
			AABB_f t_bbox_world;

			t_bbox_world.min = fmin(v0, fmin(v1, v2));
			t_bbox_world.max = fmax(v0, fmax(v1, v2));

			// Triangle bounding box in voxel grid coordinates is the world bounding box divided by the grid unit vector

			t_bbox_grid.min = convert_int3(clamp(t_bbox_world.min / *unit, (float3)(0.0f, 0.0f, 0.0f),
				convert_float3(*gridsize)));
			t_bbox_grid.max = convert_int3(clamp(t_bbox_world.max / *unit, (float3)(0.0f, 0.0f, 0.0f),
				convert_float3(*gridsize)));
        }

		// Projection test data
		if (triangleNormal.x > 0.0f) { critical_point.x = unit->x; }
		if (triangleNormal.y > 0.0f) { critical_point.y = unit->y; }
		if (triangleNormal.z > 0.0f) { critical_point.z = unit->z; }
		float d1 = dot(triangleNormal, (critical_point - v0));
		float d2 = dot(triangleNormal, ((delta_p - critical_point) - v0));

		// XY plane
		float2 n_xy_e0 = (float2)(-1.0f*e0.y, e0.x);
		float2 n_xy_e1 = (float2)(-1.0f*e1.y, e1.x);
		float2 n_xy_e2 = (float2)(-1.0f*e2.y, e2.x);
		if (triangleNormal.z < 0.0f) {
			n_xy_e0 = -n_xy_e0;
			n_xy_e1 = -n_xy_e1;
			n_xy_e2 = -n_xy_e2;
		}
		float d_xy_e0 = (-1.0f * dot(n_xy_e0, (float2)(v0.x, v0.y))) + fmax(0.0f, unit->x*n_xy_e0.x) + fmax(0.0f, unit->y*n_xy_e0.y);
		float d_xy_e1 = (-1.0f * dot(n_xy_e1, (float2)(v1.x, v1.y))) + fmax(0.0f, unit->x*n_xy_e1.x) + fmax(0.0f, unit->y*n_xy_e1.y);
		float d_xy_e2 = (-1.0f * dot(n_xy_e2, (float2)(v2.x, v2.y))) + fmax(0.0f, unit->x*n_xy_e2.x) + fmax(0.0f, unit->y*n_xy_e2.y);
		// YZ plane
		float2 n_yz_e0 = (float2)(-1.0f*e0.z, e0.y);
		float2 n_yz_e1 = (float2)(-1.0f*e1.z, e1.y);
		float2 n_yz_e2 = (float2)(-1.0f*e2.z, e2.y);
		if (triangleNormal.x < 0.0f) {
			n_yz_e0 = -n_yz_e0;
			n_yz_e1 = -n_yz_e1;
			n_yz_e2 = -n_yz_e2;
		}
		float d_yz_e0 = (-1.0f * dot(n_yz_e0, (float2)(v0.y, v0.z))) + fmax(0.0f, unit->y*n_yz_e0.x) + fmax(0.0f, unit->z*n_yz_e0.y);
		float d_yz_e1 = (-1.0f * dot(n_yz_e1, (float2)(v1.y, v1.z))) + fmax(0.0f, unit->y*n_yz_e1.x) + fmax(0.0f, unit->z*n_yz_e1.y);
		float d_yz_e2 = (-1.0f * dot(n_yz_e2, (float2)(v2.y, v2.z))) + fmax(0.0f, unit->y*n_yz_e2.x) + fmax(0.0f, unit->z*n_yz_e2.y);
		// ZX plane
		float2 n_zx_e0 = (float2)(-1.0f*e0.x, e0.z);
		float2 n_zx_e1 = (float2)(-1.0f*e1.x, e1.z);
		float2 n_zx_e2 = (float2)(-1.0f*e2.x, e2.z);
		if (triangleNormal.y < 0.0f) {
			n_zx_e0 = -n_zx_e0;
			n_zx_e1 = -n_zx_e1;
			n_zx_e2 = -n_zx_e2;
		}
		float d_xz_e0 = (-1.0f * dot(n_zx_e0, (float2)(v0.z, v0.x))) + fmax(0.0f, unit->x*n_zx_e0.x) + fmax(0.0f, unit->z*n_zx_e0.y);
		float d_xz_e1 = (-1.0f * dot(n_zx_e1, (float2)(v1.z, v1.x))) + fmax(0.0f, unit->x*n_zx_e1.x) + fmax(0.0f, unit->z*n_zx_e1.y);
		float d_xz_e2 = (-1.0f * dot(n_zx_e2, (float2)(v2.z, v2.x))) + fmax(0.0f, unit->x*n_zx_e2.x) + fmax(0.0f, unit->z*n_zx_e2.y);

		// Test possible grid boxes for overlap
		for (int z = t_bbox_grid.min.z; z < t_bbox_grid.max.z; z++){
			for (int y = t_bbox_grid.min.y; y < t_bbox_grid.max.y; y++){
				for (int x = t_bbox_grid.min.x; x < t_bbox_grid.max.x; x++){
					
					// Test for intersection of the triangle plane and the voxel
					float3 p = (float3)((float)(x) * unit->x, (float)(y) * unit->y, (float)(z) * unit->z);
					float n_dot_p = dot(triangleNormal, p);
					if ((n_dot_p + d1) * (n_dot_p + d2) > 0.0f){ continue; }
					
					// Projection tests
					// XY plane
					{
						float2 p_xy = (float2)(p.x, p.y);
						if ((dot(n_xy_e0, p_xy) + d_xy_e0) < 0.0f){ continue; }
						if ((dot(n_xy_e1, p_xy) + d_xy_e1) < 0.0f){ continue; }
						if ((dot(n_xy_e2, p_xy) + d_xy_e2) < 0.0f){ continue; }
					}
					
					// YZ plane
					{
						float2 p_yz = (float2)(p.y, p.z);
						if ((dot(n_yz_e0, p_yz) + d_yz_e0) < 0.0f){ continue; }
						if ((dot(n_yz_e1, p_yz) + d_yz_e1) < 0.0f){ continue; }
						if ((dot(n_yz_e2, p_yz) + d_yz_e2) < 0.0f){ continue; }
					}

					// XZ plane
					{
						float2 p_zx = (float2)(p.z, p.x);
						if ((dot(n_zx_e0, p_zx) + d_xz_e0) < 0.0f){ continue; }
						if ((dot(n_zx_e1, p_zx) + d_xz_e1) < 0.0f){ continue; }
						if ((dot(n_zx_e2, p_zx) + d_xz_e2) < 0.0f){ continue; }
					}

					uint location = x + (y * gridsize->x) + (z * gridsize->x * gridsize->y);
					setBit(voxel_table, location);
				}
			}
		}
		thread_id += stride;
	}
}