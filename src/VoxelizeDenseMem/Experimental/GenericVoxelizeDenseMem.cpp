
#include "VoxelizeDenseMem/Experimental/GenericVoxelizeDenseMem.hpp"

#include "Resource.hpp"

#include <OCLInfra/ErrorHandling.hpp>
#include <Math/CommonMath.hpp>

#include <cmath>
#include <cstdlib>
#include <iostream>

namespace ocla {
	namespace experimental {
		GenericVoxelizeDenseMem::GenericVoxelizeDenseMem(Resource programSource, const std::string& kernelName,
			cl::Context& context, cl::Device& device, cl::CommandQueue& commandQueue) :
			context{ context }, device{ device }, commandQueue{ commandQueue } {

			// Parse and build the OCL kernels
			std::string sourceCode = programSource.toString();
			cl::Program::Sources source;
			source.emplace_back(std::make_pair(sourceCode.c_str(), sourceCode.size()));

			cl::Program program(context, source);
			if (program.build({ device }) != CL_SUCCESS) {
				throw std::runtime_error{ "Failed to build program: BUILD LOG: " + program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) };
			}
			else if (auto buildLog = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device); !buildLog.empty()) {
				std::cout << " BUILD LOG: " << buildLog << "\n";
			}

			cl_int kernelErr = 0;
			k_voxelization = cl::Kernel{ program, kernelName.c_str(), &kernelErr };
			checkErrorAndThrow(kernelErr);

			std::cout << k_voxelization.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(device);
		}

		VoxelGridBuffer<cl_uint> GenericVoxelizeDenseMem::voxelize(const BufferWrapper<cl_float>& d_triangles, const cl_uint numTriangles,
			const cl_uint3 gridDimensions, const AABB<cl_float3> worldBoundingBox, const cl_float3 unitVoxelDimensions) {

			// Create voxel grid buffer. We require 1 bit per voxel, hence dividing the number of voxels by 8 to get the 
			// required memory size in bytes.
			const cl_uint numVoxels = gridDimensions.x * gridDimensions.y * gridDimensions.z;

			// Calculate the minimum bytes required to store 1 bit per voxel
			auto requiredMem = ceilUIntDiv((long)numVoxels, (long)8);
			// Pad with additional bytes due to the block size being used
			if (auto remainder = requiredMem % sizeof(cl_uint); remainder != 0) {
				requiredMem += sizeof(cl_uint) - (requiredMem % sizeof(cl_uint));
			}

			cl_int bufferCtorErr = 0;
			cl::Buffer d_voxels(context, CL_MEM_READ_WRITE, requiredMem, nullptr, &bufferCtorErr);
			checkErrorAndThrow(bufferCtorErr);

			// initialize to zero
			commandQueue.enqueueFillBuffer<cl_uint>(d_voxels, 0, 0, requiredMem);
			commandQueue.finish();

			VoxelGridBuffer<cl_uint> voxelGridBuffer{ d_voxels, gridDimensions, worldBoundingBox, unitVoxelDimensions };

			voxelize(d_triangles, numTriangles, voxelGridBuffer);

			return VoxelGridBuffer<cl_uint>{d_voxels, gridDimensions, worldBoundingBox, unitVoxelDimensions};
		}

		void GenericVoxelizeDenseMem::voxelize(const BufferWrapper<cl_float>& d_triangles, const cl_uint numTriangles,
			VoxelGridBuffer<cl_uint>& voxelGrid) {

			// Calculate the number of work-items
			size_t numWorkItems;
			{
				size_t remainder = numTriangles % processorDensity;
				numWorkItems = (numTriangles / processorDensity) * processorDensity;
				numWorkItems += (remainder != 0) ? processorDensity : 0;
			}

			cl_int bufferCtorErr = 0;
			cl::Buffer d_bbox(context, CL_MEM_READ_WRITE, sizeof(AABB<cl_float3>), nullptr, &bufferCtorErr);
			checkErrorAndThrow(bufferCtorErr);

			cl::Buffer d_gridsize(context, CL_MEM_READ_WRITE, sizeof(cl_uint3), nullptr, &bufferCtorErr);
			checkErrorAndThrow(bufferCtorErr);

			cl::Buffer d_n_triangles(context, CL_MEM_READ_WRITE, sizeof(cl_uint), nullptr, &bufferCtorErr);
			checkErrorAndThrow(bufferCtorErr);

			cl::Buffer d_unit(context, CL_MEM_READ_WRITE, sizeof(cl_float3), nullptr, &bufferCtorErr);
			checkErrorAndThrow(bufferCtorErr);

			{
				checkErrorAndThrow(commandQueue.enqueueWriteBuffer(d_bbox, true, 0, sizeof(AABB<cl_float3>), &voxelGrid.worldBoundBox));
				checkErrorAndThrow(commandQueue.enqueueWriteBuffer(d_gridsize, true, 0, sizeof(cl_uint3), &voxelGrid.gridDimensions));
				checkErrorAndThrow(commandQueue.enqueueWriteBuffer(d_n_triangles, true, 0, sizeof(cl_uint), &numTriangles));
				checkErrorAndThrow(commandQueue.enqueueWriteBuffer(d_unit, true, 0, sizeof(cl_float3), &voxelGrid.unitVoxelDimensions));
				commandQueue.finish();
			}

			checkErrorAndThrow(k_voxelization.setArg(0, d_bbox));
			checkErrorAndThrow(k_voxelization.setArg(1, d_gridsize));
			checkErrorAndThrow(k_voxelization.setArg(2, d_n_triangles));
			checkErrorAndThrow(k_voxelization.setArg(3, d_unit));
			checkErrorAndThrow(k_voxelization.setArg(4, d_triangles));
			checkErrorAndThrow(k_voxelization.setArg(5, voxelGrid.voxelGrid));

			// voxelize
			checkErrorAndThrow(commandQueue.enqueueNDRangeKernel(k_voxelization, cl::NullRange, cl::NDRange(numWorkItems),
				cl::NDRange(processorDensity / 2)));
			commandQueue.finish();
		}
	} // namespace experimental
} // namespace ocla