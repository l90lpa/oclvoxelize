
#include "VoxelizeDenseMem/VoxelGridToVoxelList.hpp"

#include <cmath>

namespace ocla {

	std::vector<cl_int3> extractIndividualVoxels(const std::vector<cl_uint>& voxelGrid, const size_t numVoxels, const size_t axisResolution) {

		std::vector<cl_int3> voxels;

		for (cl_uint i = 0; i < voxelGrid.size(); ++i) {
			cl_uint int_location = i;

			for (size_t j = 0; j < 32; ++j) {
				cl_uint location_1d = i * 32 + j;

				cl_uint bit_pos = 31 - (location_1d % 32); // we count bit positions RtL, but array indices LtR
				cl_uint mask = 1 << bit_pos;

				bool bit = voxelGrid[int_location] & mask;
				if (bit) {
					auto voxel = determineVoxelCoord(location_1d, axisResolution);
					voxels.emplace_back(std::move(voxel));
				}
			}
		}

		return voxels;
	}

	cl_int3 determineVoxelCoord(cl_int location_1d, const size_t axisResolution) {
		cl_int z = location_1d / static_cast<size_t>(std::pow(axisResolution, 2));
		location_1d -= z * static_cast<size_t>(std::pow(axisResolution, 2));

		cl_int y = location_1d / axisResolution;
		location_1d -= y * axisResolution;

		cl_int x = location_1d;

		return cl_int3{ x,y,z };
	}


} // ocla