
typedef struct {
    float3 min;
    float3 max;
} AABB_f;

typedef struct {
    int3 min;
    int3 max;
} AABB_i;


inline uint bitBlockLocalIndex(uint location) {
#ifdef BLOCK_INDEXING_RTL
	return (uint)(31) - (location % (uint)(32)); // we count bit positions RtL, but array indices LtR
#else
	return location % (uint)(32); // we count bit positions LtR, and array indices LtR
#endif
}

inline uint blockIndex(uint location) {
	return location / 32;
}

void setBit(__global uint* voxel_table, uint location) {
	uint bit_pos = bitBlockLocalIndex(location);
	uint mask = 1 << bit_pos;
	atomic_or(&(voxel_table[blockIndex(location)]), mask);  // location / 32 == block index
}

void flipBit(__global uint* voxel_table, uint location) {
	uint bit_pos = bitBlockLocalIndex(location);
	uint mask = 1 << bit_pos;
	atomic_xor(&(voxel_table[blockIndex(location)]), mask); // location / 32 == block index
}


// [Schwarz and Seidel 2010]
__kernel void denseSolidVoxelize(__constant AABB_f* bbox, __constant uint3* gridsize, __constant uint* n_triangles, __constant float3* unit,
	__global float* triangle_data, __global uint* voxel_table) {

	uint thread_id = get_global_id(0);
	uint stride = get_global_size(0);

	while (thread_id < *n_triangles) { // Every thread works on a specific triangle
		uint triangle_id = thread_id * 9; // Each triangle contains 3 vertices each with 3 components

		// Move vertices to origin using bbox
		float3 v0 = (float3)(triangle_data[triangle_id], triangle_data[triangle_id + 1], triangle_data[triangle_id + 2]) - bbox->min;
		float3 v1 = (float3)(triangle_data[triangle_id + 3], triangle_data[triangle_id + 4], triangle_data[triangle_id + 5]) - bbox->min;
		float3 v2 = (float3)(triangle_data[triangle_id + 6], triangle_data[triangle_id + 7], triangle_data[triangle_id + 8]) - bbox->min;

		// Edge vectors
		float3 e0 = v1 - v0;
		float3 e1 = v2 - v1;
		float3 e2 = v0 - v2;

		// Normal vector pointing outward from the triangle
		float3 triangleNormal = normalize(cross(v1 - v0, v2 - v1));

		// Compute the triangles integer bounding box
		AABB_i t_bbox_grid;
		{
			AABB_f t_bbox_world;

			t_bbox_world.min = fmin(v0, fmin(v1, v2));
			t_bbox_world.max = fmax(v0, fmax(v1, v2));

			// Triangle bounding box in voxel grid coordinates is the world bounding box divided by the grid unit vector

			t_bbox_grid.min = convert_int3(clamp(t_bbox_world.min / *unit, (float3)(0.0f, 0.0f, 0.0f), convert_float3(*gridsize)));
			t_bbox_grid.max = convert_int3(clamp(t_bbox_world.max / *unit, (float3)(0.0f, 0.0f, 0.0f), convert_float3(*gridsize)));
		}

		// Projection test data
		// YZ plane
		float2 n_yz_e0 = (float2)(-1.0f * e0.z, e0.y);
		float2 n_yz_e1 = (float2)(-1.0f * e1.z, e1.y);
		float2 n_yz_e2 = (float2)(-1.0f * e2.z, e2.y);
		if (triangleNormal.x < 0.0f) {
			n_yz_e0 = -n_yz_e0;
			n_yz_e1 = -n_yz_e1;
			n_yz_e2 = -n_yz_e2;
		}
		float d_yz_e0 = (-1.0f * dot(n_yz_e0, (float2)(v0.y, v0.z)));
		float d_yz_e1 = (-1.0f * dot(n_yz_e1, (float2)(v1.y, v1.z)));
		float d_yz_e2 = (-1.0f * dot(n_yz_e2, (float2)(v2.y, v2.z)));

		// Test possible voxel columns for overlap
		for (int z = t_bbox_grid.min.z; z <= t_bbox_grid.max.z; z++) {
			for (int y = t_bbox_grid.min.y; y <= t_bbox_grid.max.y; y++) {

				float2 p_yz = (float2)(((float)(y)+0.5) * unit->y, ((float)(z)+0.5) * unit->z);

				float f_yz_e0 = 0.0f;
				float f_yz_e1 = 0.0f;
				float f_yz_e2 = 0.0f;
				if (n_yz_e0.x > 0 || (n_yz_e0.x == 0 && n_yz_e0.y < 0)) { f_yz_e0 = 10.0f * FLT_MIN; }
				if (n_yz_e1.x > 0 || (n_yz_e1.x == 0 && n_yz_e1.y < 0)) { f_yz_e1 = 10.0f * FLT_MIN; }
				if (n_yz_e2.x > 0 || (n_yz_e2.x == 0 && n_yz_e2.y < 0)) { f_yz_e2 = 10.0f * FLT_MIN; }

				if ((dot(n_yz_e0, p_yz) + d_yz_e0) + f_yz_e0 <= 0.0f) { continue; }
				if ((dot(n_yz_e1, p_yz) + d_yz_e1) + f_yz_e1 <= 0.0f) { continue; }
				if ((dot(n_yz_e2, p_yz) + d_yz_e2) + f_yz_e2 <= 0.0f) { continue; }

				if (triangleNormal.x == 0.0f) { continue; }
				// Using dot(triangleNormal, (1,0,0)) == triangleNormal.x
				int x = trunc((dot(triangleNormal, (v0 - (float3)(0, p_yz))) / triangleNormal.x) / unit->x); 

				// flip all of the voxels from the identified voxel to the end
				for (; x < gridsize->x; x++) {

					// TODO: improve perf by caching bit flips to a 32 bit unit local to the thread 
					// and then write this in one.

					uint location = x + (y * gridsize->x) + (z * gridsize->x * gridsize->y);

					flipBit(voxel_table, location);
				}
			}
		}
		thread_id += stride;
	}
}